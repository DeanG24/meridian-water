# Meridian Water VR

In this repository you will find the source code, assets and project settings of the Meridian Water VR (Unity 2018.3.11f1 recommended).

---

## NOTICE

> The master branch contains our most stable release.
> The facades development branch contains the latest features and work-flows awaiting technical review.

---

## Roadmap

> Roadmap is subject to change. Last reviewed 9th of April 2019.

| Version | Defining Feature                                                        |
| ------- | ----------------------------------------------------------------------- |
| 1.0     | ?????                           										|

---

## History

| Version | Defining Feature                                                        |
| ------- | ----------------------------------------------------------------------- |
| 0.1     | ?????											                   		|

---

## Contacts

For more information contact:

* Jak Hussain (Programmer) - jak.hussain@arup.com
* Dean Giddy (Programmer) - dean.giddy@arup.com
* Nathan Lennox (Designer) - nathan.lennox@arup.com
